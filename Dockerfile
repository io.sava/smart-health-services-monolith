FROM adoptopenjdk:11-jre-hotspot

ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} application.jar

ENTRYPOINT ["java", "-Dserver.port=$PORT -Dspring.profiles.active=heroku -jar application.jar"]