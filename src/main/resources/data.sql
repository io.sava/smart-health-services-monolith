-- hospitals
insert into hospitals(name, address, fax, email, phone)
values ('Spitalul Clinic Judetean de Urgenta Sf. Spiridon Iasi', 'Bd. Independenței nr. 1, cod 700111, IAȘI', '0232-217781', 'contact@spitalspiridon.ro', '0232-240822');
insert into hospitals(name, address, fax, email, phone)
values ('Spitalul Clinic de Boli Infectioase Sfanta Parascheva Iasi', 'Str. Octav Botez nr. 2, Iasi, Iasi', '0232 264 252', 'sbc_iasi@mail.dntis.ro', '0232 267 719');
insert into hospitals(name, address, fax, email, phone)
values ('Spitalul Clinic de Urgenta Militar Dr. Iacob Czihac Iasi', 'Str. Berthelot Henri Mathias nr. 7-9, Iasi, Iasi', '0232 216 844', 'smuis32@yahoo.com', ' 0733 981 480');

-- departments
insert into departments(name, hospital_id)
values ('Medicina interna', 1);
insert into departments(name, hospital_id)
values ('Chirurgie generala', 1);
insert into departments(name, hospital_id)
values ('Ortopedie si traumatologie', 1);
insert into departments(name, hospital_id)
values ('Oftalmologie', 1);
insert into departments(name, hospital_id)
values ('ORL', 1);
insert into departments(name, hospital_id)
values ('Dermato-Venerologie', 1);

insert into departments(name, hospital_id)
values ('Medicina interna', 2);
insert into departments(name, hospital_id)
values ('Chirurgie generala', 2);
insert into departments(name, hospital_id)
values ('Ortopedie si traumatologie', 2);
insert into departments(name, hospital_id)
values ('Oftalmologie', 2);
insert into departments(name, hospital_id)
values ('ORL', 2);
insert into departments(name, hospital_id)
values ('Dermato-Venerologie', 2);

insert into departments(name, hospital_id)
values ('Medicina interna', 3);
insert into departments(name, hospital_id)
values ('Chirurgie generala', 3);
insert into departments(name, hospital_id)
values ('Ortopedie si traumatologie', 3);
insert into departments(name, hospital_id)
values ('Oftalmologie', 3);
insert into departments(name, hospital_id)
values ('ORL', 3);
insert into departments(name, hospital_id)
values ('Dermato-Venerologie', 3);

-- doctors
insert into doctors(name, position, email, department_id)
values ('Virgil Laurentiu', 'Medic specialist', 'virgil@gmail.com', 1);
insert into doctors(name, position, email, department_id)
values ('Mirel Titirez', 'Medic rezident', 'mirel@gmail.com', 2);
insert into doctors(name, position, email, department_id)
values ('Emanuel Gheorghe', 'Medic specialist', 'emanuel@gmail.com', 3);
insert into doctors(name, position, email, department_id)
values ('Arsenie Romanescu', 'Medic rezident', 'arsenie@gmail.com', 2);
insert into doctors(name, position, email, department_id)
values ('Iulien Grigorescu', 'Medic primar', 'iulien@gmail.com', 1);
insert into doctors(name, position, email, department_id)
values ('Nicusor Luca', 'Medic specialist', 'nicusor@gmail.com', 4);
insert into doctors(name, position, email, department_id)
values ('Calin Balcescu', 'Medic primar', 'calin@gmail.com', 5);
insert into doctors(name, position, email, department_id)
values ('Miron Stancu', 'Medic rezident', 'miron@gmail.com', 3);
insert into doctors(name, position, email, department_id)
values ('Jan Muresan', 'Medic specialist', 'jan@gmail.com', 6);

insert into doctors(name, position, email, department_id)
values ('Luca Dumitru', 'Medic specialist', 'luca@gmail.com', 7);
insert into doctors(name, position, email, department_id)
values ('Dorin Galca', 'Medic specialist', 'dorin@gmail.com', 7);
insert into doctors(name, position, email, department_id)
values ('Gabriel Enache', 'Medic rezident', 'gabriel@gmail.com', 8);
insert into doctors(name, position, email, department_id)
values ('Paul Puscas', 'Medic specialist', 'paul@gmail.com', 9);
insert into doctors(name, position, email, department_id)
values ('Bogdan Andreescu', 'Medic rezident', 'bogdan@gmail.com', 8);
insert into doctors(name, position, email, department_id)
values ('Matei Barbu', 'Medic primar', 'matei@gmail.com', 7);
insert into doctors(name, position, email, department_id)
values ('Toma Zamfir', 'Medic specialist', 'toma@gmail.com', 10);
insert into doctors(name, position, email, department_id)
values ('Sebastian Raceanu', 'Medic primar', 'sebastian@gmail.com', 11);
insert into doctors(name, position, email, department_id)
values ('Teodosie Ilionescu', 'Medic rezident', 'teodosie@gmail.com', 9);
insert into doctors(name, position, email, department_id)
values ('Raul Plesu', 'Medic specialist', 'raul@gmail.com', 12);

insert into doctors(name, position, email, department_id)
values ('Sergiu Gheorghiu', 'Medic specialist', 'sergiu@gmail.com', 13);
insert into doctors(name, position, email, department_id)
values ('Cezar Lupul', 'Medic rezident', 'cezar@gmail.com', 13);
insert into doctors(name, position, email, department_id)
values ('Valerian Roman', 'Medic specialist', 'valerian@gmail.com', 14);
insert into doctors(name, position, email, department_id)
values ('Jean Negrescu', 'Medic rezident', 'jean@gmail.com', 15);
insert into doctors(name, position, email, department_id)
values ('Ion Pecurar', 'Medic primar', 'ion@gmail.com', 14);
insert into doctors(name, position, email, department_id)
values ('Serghei Alexandrescu', 'Medic specialist', 'serghei@gmail.com', 13);
insert into doctors(name, position, email, department_id)
values ('Geza Niculescu', 'Medic primar', 'geza@gmail.com', 16);
insert into doctors(name, position, email, department_id)
values ('Tudor Serban', 'Medic rezident', 'tudor@gmail.com', 17);
insert into doctors(name, position, email, department_id)
values ('Silviu Arcos', 'Medic specialist', 'silviu@gmail.com', 15);
insert into doctors(name, position, email, department_id)
values ('Costel Avramescu', 'Medic specialist', 'costel@gmail.com', 18);

-- medications
insert into medications(name, brand)
values ('Ibuprofen', 'Cipla');
insert into medications(name, brand)
values ('FLUOROURACIL', 'EBEWE');
insert into medications(name, brand)
values ('ABACAVIR', 'TERAPIA');
insert into medications(name, brand)
values ('ABASAGLAR', 'LILLY FRANCE');
insert into medications(name, brand)
values ('ALOPURINOL AUROBINDO', 'AUROBINDO PHARMA ROMANIA');
insert into medications(name, brand)
values ('SIMVASTATIN AUROBINDO', 'AUROBINDO PHARMA ROMANIA');
insert into medications(name, brand)
values ('OMEPRAZOL TERAPIA', 'TERAPIA SA');
insert into medications(name, brand)
values ('TICLODIN', 'AC HELCOR SRL');
insert into medications(name, brand)
values ('CARVEDILOL SANDOZ', 'HEXAL AG');
insert into medications(name, brand)
values ('CORYOL', 'KRKA D.D.');

-- rooms
insert into rooms(type, floor, hospital_id)
values ('ATI', 1, 1);
insert into rooms(type, floor, hospital_id)
values ('Salon', 1, 1);
insert into rooms(type, floor, hospital_id)
values ('Salon', 2, 1);

insert into rooms(type, floor, hospital_id)
values ('ATI', 2, 2);
insert into rooms(type, floor, hospital_id)
values ('Salon', 2, 2);
insert into rooms(type, floor, hospital_id)
values ('Salon', 1, 2);

insert into rooms(type, floor, hospital_id)
values ('ATI', 2, 3);
insert into rooms(type, floor, hospital_id)
values ('Salon', 2, 3);
insert into rooms(type, floor, hospital_id)
values ('Salon', 1, 3);

-- nurses
insert into nurses(name, position, email, department_id)
values('Angela Cornea', 'Asistent medical generalist', 'angela@gmail.com', 1);
insert into nurses(name, position, email, department_id)
values('Ionela Gheorghe', 'Asistent medical generalist', 'ionela@gmail.com', 3);
insert into nurses(name, position, email, department_id)
values('Miruna Vulpes', 'Asistent medical generalist', 'miruna@gmail.com', 7);
insert into nurses(name, position, email, department_id)
values('Felicia Bogza', 'Asistent medical generalist', 'felicia@gmail.com', 10);
insert into nurses(name, position, email, department_id)
values('Loredana Grigorescu', 'Asistent medical generalist', 'loredana@gmail.com', 13);
insert into nurses(name, position, email, department_id)
values('Veronica Vasile', 'Asistent medical generalist', 'veronica@gmail.com', 14);
insert into nurses(name, position, email, department_id)
values('Celestina Tugurlan', 'Asistent medical generalist', 'celestina@gmail.com', 16);

-- patients
insert into patients(cnp, name, age, address, phone, email)
values('1990731336389', 'Sava Ioan', 21, 'Falticeni, Suceava', '0741987734', 'ioan.sava@gmail.com');
insert into patients(cnp, name, age, address, phone, email)
values('1990731336388', 'Sava Ionescu', 21, 'Falticeni, Suceava', '0741987734', 'io.sava@gmail.com');

-- procedures
insert into procedures(name, cost)
values('Consultatie de specialitate medicina interna', 250);
insert into procedures(name, cost)
values('Ecografie abdominala generala', 300);

insert into procedures(name, cost)
values('Apendicectomie clasica', 4000);
insert into procedures(name, cost)
values('Abces hepatic – drenaj', 4000);

insert into procedures(name, cost)
values('Osteotomia şoldului', 300);
insert into procedures(name, cost)
values('Artroscopia umărului', 500);

insert into procedures(name, cost)
values('Masurare tensiune intraoculara', 100);
insert into procedures(name, cost)
values('Gonioscopie', 150);

insert into procedures(name, cost)
values('Fibroscopie nazală', 100);
insert into procedures(name, cost)
values('Exsudat faringian', 150);

insert into procedures(name, cost)
values('Dermatoscopie leziuni pigmentare', 100);
insert into procedures(name, cost)
values('Electrocauterizare', 50);

-- users
insert into users(email, owner_id, password, role)
values('admin@gmail.com', null, '$2a$10$NcN86PLYIVpljusXZv/jDuy9b8JXDQDNmY2QcrvkHT49PA3riR5na', 'ADMIN');

insert into users(email, owner_id, password, role)
values('virgil@gmail.com', 1, '$2a$10$2OkJ6SS7J2a.55IZC334ROcpFT365GoKZNDRXopXNev53oHHqQk6K', 'DOCTOR');
insert into users(email, owner_id, password, role)
values('mirel@gmail.com', 2, '$2a$10$Am5mqIraKglC8p1z6zdtdOfyJBrBEZ8iJGxRfT44R9iBmWD0YDsl6', 'DOCTOR');

insert into users(email, owner_id, password, role)
values('angela@gmail.com', 1, '$2a$10$Ne1MUbt945493To.2yhkLOIRjmcJZsF7c/Mn76fQV.1HW8QAgbb4i', 'NURSE');

insert into users(email, owner_id, password, role)
values('ioan.sava@gmail.com', 1, '$2a$10$O8pfw3H3e.KvREtXJ6kreuIf9UxXUH4oPKd56Mi033mYG8th1ckOK', 'PATIENT');
insert into users(email, owner_id, password, role)
values('io.sava@gmail.com', 2, '$2y$10$6fi/5a8ZV2fybHxuCF.xm.xKB3mMvYI2lTEcHpCAx1AThjqJiG3Ju', 'PATIENT');

-- stays
insert into stays(patient_id, room_id, start_date, end_date)
values(1, 1, '2020-08-20', '2020-08-27');
insert into stays(patient_id, room_id, start_date, end_date)
values(1, 2, '2020-09-20', '2020-09-27');
insert into stays(patient_id, room_id, start_date, end_date)
values(2, 1, '2020-08-20', '2020-08-27');
insert into stays(patient_id, room_id, start_date, end_date)
values(2, 2, '2020-09-20', '2020-09-27');

-- undergoes
insert into undergoes(doctor_id, procedure_id, stay_id)
values(1, 2, 1);
insert into undergoes(doctor_id, procedure_id, stay_id)
values(1, 2, 2);
insert into undergoes(doctor_id, procedure_id, stay_id)
values(1, 2, 3);
insert into undergoes(doctor_id, procedure_id, stay_id)
values(1, 2, 4);

-- appointments
insert into appointments(date, diagnosis, doctor_id, nurse_id, patient_id)
values('2020-08-19', 'Sanatos', 1, 1, 1);
insert into appointments(date, diagnosis, doctor_id, nurse_id, patient_id)
values('2020-09-19', '-', 2, 2, 1);
insert into appointments(date, diagnosis, doctor_id, nurse_id, patient_id)
values('2020-08-19', '-', 1, 3, 2);
insert into appointments(date, diagnosis, doctor_id, nurse_id, patient_id)
values('2020-09-19', '-', 3, 4, 2);

-- prescriptions
insert into prescriptions(dose, appointment_id, medication_id)
values('25 mg', 1, 1);
insert into prescriptions(dose, appointment_id, medication_id)
values('100 mg', 1, 2);
insert into prescriptions(dose, appointment_id, medication_id)
values('75 mg', 3, 3);
insert into prescriptions(dose, appointment_id, medication_id)
values('20 mg', 3, 4);
insert into prescriptions(dose, appointment_id, medication_id)
values('75 mg', 4, 3);
insert into prescriptions(dose, appointment_id, medication_id)
values('20 mg', 4, 5);
insert into prescriptions(dose, appointment_id, medication_id)
values('20 mg', 4, 6);
