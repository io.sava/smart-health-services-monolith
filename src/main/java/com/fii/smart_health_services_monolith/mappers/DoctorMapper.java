package com.fii.smart_health_services_monolith.mappers;

import com.fii.smart_health_services_monolith.dtos.DoctorDto;
import com.fii.smart_health_services_monolith.entities.Doctor;
import com.fii.smart_health_services_monolith.services.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class DoctorMapper {
    private final DepartmentService departmentService;

    public Doctor toDoctor(DoctorDto doctorDto) {
        Doctor doctor = new Doctor();
        doctor.setName(doctorDto.getName());
        doctor.setPosition(doctorDto.getPosition());
        doctor.setEmail(doctorDto.getEmail());
        doctor.setDepartment(departmentService.getById(doctorDto.getDepartmentId()));

        return doctor;
    }
}
