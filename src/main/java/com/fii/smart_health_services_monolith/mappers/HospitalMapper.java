package com.fii.smart_health_services_monolith.mappers;

import com.fii.smart_health_services_monolith.dtos.HospitalDto;
import com.fii.smart_health_services_monolith.entities.Hospital;
import org.springframework.stereotype.Component;

@Component
public final class HospitalMapper {
    public Hospital toHospital(HospitalDto hospitalDto) {
        Hospital hospital = new Hospital();
        hospital.setName(hospitalDto.getName());
        hospital.setAddress(hospitalDto.getAddress());
        hospital.setFax(hospitalDto.getFax());
        hospital.setEmail(hospitalDto.getEmail());
        hospital.setPhone(hospitalDto.getPhone());

        return hospital;
    }
}
