package com.fii.smart_health_services_monolith.mappers;

import com.fii.smart_health_services_monolith.dtos.PrescriptionDto;
import com.fii.smart_health_services_monolith.entities.Prescription;
import com.fii.smart_health_services_monolith.services.AppointmentService;
import com.fii.smart_health_services_monolith.services.MedicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class PrescriptionMapper {
    private final AppointmentService appointmentService;
    private final MedicationService medicationService;

    public Prescription toPrescription(PrescriptionDto prescriptionDto) {
        Prescription prescription = new Prescription();
        prescription.setDose(prescriptionDto.getDose());
        prescription.setAppointment(appointmentService.getById(prescriptionDto.getAppointmentId()));
        prescription.setMedication(medicationService.getById(prescriptionDto.getMedicationId()));

        return prescription;
    }
}
