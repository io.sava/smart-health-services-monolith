package com.fii.smart_health_services_monolith.mappers;

import com.fii.smart_health_services_monolith.dtos.DepartmentDto;
import com.fii.smart_health_services_monolith.entities.Department;
import com.fii.smart_health_services_monolith.services.HospitalService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class DepartmentMapper {
    private final HospitalService hospitalService;

    public Department toDepartment(DepartmentDto departmentDto) {
        Department department = new Department();
        department.setName(departmentDto.getName());
        department.setHospital(hospitalService.getById(departmentDto.getHospitalId()));

        return department;
    }
}
