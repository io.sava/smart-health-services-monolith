package com.fii.smart_health_services_monolith.mappers;

import com.fii.smart_health_services_monolith.dtos.AppointmentDto;
import com.fii.smart_health_services_monolith.entities.Appointment;
import com.fii.smart_health_services_monolith.services.DoctorService;
import com.fii.smart_health_services_monolith.services.NurseService;
import com.fii.smart_health_services_monolith.services.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class AppointmentMapper {
    private final DoctorService doctorService;
    private final NurseService nurseService;
    private final PatientService patientService;

    public Appointment toAppointment(AppointmentDto appointmentDto) {
        Appointment appointment = new Appointment();
        appointment.setDate(appointmentDto.getDate());
        appointment.setDiagnosis(appointmentDto.getDiagnosis());
        appointment.setDoctor(doctorService.getById(appointmentDto.getDoctorId()));
        appointment.setNurse(nurseService.getById(appointmentDto.getNurseId()));
        appointment.setPatient(patientService.getById(appointmentDto.getPatientId()));

        return appointment;
    }
}
