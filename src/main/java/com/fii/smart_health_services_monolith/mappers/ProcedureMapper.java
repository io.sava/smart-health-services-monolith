package com.fii.smart_health_services_monolith.mappers;

import com.fii.smart_health_services_monolith.dtos.ProcedureDto;
import com.fii.smart_health_services_monolith.entities.Procedure;
import org.springframework.stereotype.Component;

@Component
public final class ProcedureMapper {
    public Procedure toProcedure(ProcedureDto procedureDto) {
        Procedure procedure = new Procedure();
        procedure.setName(procedureDto.getName());
        procedure.setCost(procedureDto.getCost());

        return procedure;
    }
}
