package com.fii.smart_health_services_monolith.mappers;

import com.fii.smart_health_services_monolith.dtos.users.UserRegisterDto;
import com.fii.smart_health_services_monolith.entities.Role;
import com.fii.smart_health_services_monolith.entities.User;
import com.fii.smart_health_services_monolith.exceptions.InvalidRoleException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class UserMapper {
    private final PasswordEncoder passwordEncoder;

    public User toUser(UserRegisterDto userRegisterDto) {
        User user = new User();
        user.setEmail(userRegisterDto.getEmail());
        user.setPassword(passwordEncoder.encode(userRegisterDto.getPassword()));
        user.setOwnerId(userRegisterDto.getOwnerId());

        try {
            user.setRole(Role.valueOf(userRegisterDto.getRole()));
        } catch (IllegalArgumentException exception) {
            throw new InvalidRoleException(userRegisterDto.getRole());
        }

        return user;
    }
}
