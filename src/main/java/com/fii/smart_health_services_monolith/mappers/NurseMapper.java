package com.fii.smart_health_services_monolith.mappers;

import com.fii.smart_health_services_monolith.dtos.NurseDto;
import com.fii.smart_health_services_monolith.entities.Nurse;
import com.fii.smart_health_services_monolith.services.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class NurseMapper {
    private final DepartmentService departmentService;

    public Nurse toNurse(NurseDto nurseDto) {
        Nurse nurse = new Nurse();
        nurse.setName(nurseDto.getName());
        nurse.setPosition(nurseDto.getPosition());
        nurse.setEmail(nurseDto.getEmail());
        nurse.setDepartment(departmentService.getById(nurseDto.getDepartmentId()));

        return nurse;
    }
}
