package com.fii.smart_health_services_monolith.mappers;

import com.fii.smart_health_services_monolith.dtos.StayDto;
import com.fii.smart_health_services_monolith.entities.Stay;
import com.fii.smart_health_services_monolith.services.PatientService;
import com.fii.smart_health_services_monolith.services.RoomService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class StayMapper {
    private final PatientService patientService;
    private final RoomService roomService;

    public Stay toStay(StayDto stayDto) {
        Stay stay = new Stay();
        stay.setStartDate(stayDto.getStartDate());
        stay.setEndDate(stayDto.getEndDate());
        stay.setPatient(patientService.getById(stayDto.getPatientId()));
        stay.setRoom(roomService.getById(stayDto.getRoomId()));

        return stay;
    }
}
