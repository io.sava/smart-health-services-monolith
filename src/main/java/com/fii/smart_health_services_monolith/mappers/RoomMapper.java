package com.fii.smart_health_services_monolith.mappers;

import com.fii.smart_health_services_monolith.dtos.RoomDto;
import com.fii.smart_health_services_monolith.entities.Room;
import com.fii.smart_health_services_monolith.services.HospitalService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class RoomMapper {
    private final HospitalService hospitalService;

    public Room toRoom(RoomDto roomDto) {
        Room room = new Room();
        room.setFloor(roomDto.getFloor());
        room.setType(roomDto.getType());
        room.setHospital(hospitalService.getById(roomDto.getHospitalId()));

        return room;
    }
}
