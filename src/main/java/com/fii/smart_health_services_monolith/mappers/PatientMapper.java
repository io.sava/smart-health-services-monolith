package com.fii.smart_health_services_monolith.mappers;

import com.fii.smart_health_services_monolith.dtos.PatientDto;
import com.fii.smart_health_services_monolith.entities.Patient;
import org.springframework.stereotype.Component;

@Component
public final class PatientMapper {
    public Patient toPatient(PatientDto patientDto) {
        Patient patient = new Patient();
        patient.setCnp(patientDto.getCnp());
        patient.setName(patientDto.getName());
        patient.setAddress(patientDto.getAddress());
        patient.setAge(patientDto.getAge());
        patient.setPhone(patientDto.getPhone());
        patient.setEmail(patientDto.getEmail());

        return patient;
    }
}
