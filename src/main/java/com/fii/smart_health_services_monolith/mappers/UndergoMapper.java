package com.fii.smart_health_services_monolith.mappers;

import com.fii.smart_health_services_monolith.dtos.UndergoDto;
import com.fii.smart_health_services_monolith.entities.Undergo;
import com.fii.smart_health_services_monolith.services.DoctorService;
import com.fii.smart_health_services_monolith.services.ProcedureService;
import com.fii.smart_health_services_monolith.services.StayService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public final class UndergoMapper {
    private final DoctorService doctorService;
    private final StayService stayService;
    private final ProcedureService procedureService;

    public Undergo toUndergo(UndergoDto undergoDto) {
        Undergo undergo = new Undergo();
        undergo.setDoctor(doctorService.getById(undergoDto.getDoctorId()));
        undergo.setStay(stayService.getById(undergoDto.getStayId()));
        undergo.setProcedure(procedureService.getById(undergoDto.getProcedureId()));

        return undergo;
    }
}
