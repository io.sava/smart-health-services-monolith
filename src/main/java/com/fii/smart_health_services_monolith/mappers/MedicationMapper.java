package com.fii.smart_health_services_monolith.mappers;

import com.fii.smart_health_services_monolith.dtos.MedicationDto;
import com.fii.smart_health_services_monolith.entities.Medication;
import org.springframework.stereotype.Component;

@Component
public final class MedicationMapper {
    public Medication toMedication(MedicationDto medicationDto) {
        Medication medication = new Medication();
        medication.setName(medicationDto.getName());
        medication.setBrand(medicationDto.getBrand());

        return medication;
    }
}
