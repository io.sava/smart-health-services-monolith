package com.fii.smart_health_services_monolith.repositories;

import com.fii.smart_health_services_monolith.entities.Prescription;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PrescriptionRepository extends JpaRepository<Prescription, Long> {
}
