package com.fii.smart_health_services_monolith.repositories;

import com.fii.smart_health_services_monolith.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientRepository extends JpaRepository<Patient, Long> {
}
