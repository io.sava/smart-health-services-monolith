package com.fii.smart_health_services_monolith.repositories;

import com.fii.smart_health_services_monolith.entities.Undergo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UndergoRepository extends JpaRepository<Undergo, Long> {
    Page<Undergo> findByDoctorId(Long doctorId, Pageable pageable);
}
