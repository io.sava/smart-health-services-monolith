package com.fii.smart_health_services_monolith.repositories;

import com.fii.smart_health_services_monolith.entities.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppointmentRepository extends JpaRepository<Appointment, Long> {
}
