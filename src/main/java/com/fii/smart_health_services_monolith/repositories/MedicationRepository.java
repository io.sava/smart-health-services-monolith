package com.fii.smart_health_services_monolith.repositories;

import com.fii.smart_health_services_monolith.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationRepository extends JpaRepository<Medication, Long> {
}
