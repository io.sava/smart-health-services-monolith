package com.fii.smart_health_services_monolith.repositories;

import com.fii.smart_health_services_monolith.entities.Stay;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StayRepository extends JpaRepository<Stay, Long> {
}
