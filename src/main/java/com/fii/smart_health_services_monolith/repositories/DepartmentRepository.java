package com.fii.smart_health_services_monolith.repositories;

import com.fii.smart_health_services_monolith.entities.Department;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
}
