package com.fii.smart_health_services_monolith.repositories;

import com.fii.smart_health_services_monolith.entities.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorRepository extends JpaRepository<Doctor, Long> {
}
