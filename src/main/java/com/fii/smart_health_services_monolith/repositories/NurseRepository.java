package com.fii.smart_health_services_monolith.repositories;

import com.fii.smart_health_services_monolith.entities.Nurse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NurseRepository extends JpaRepository<Nurse, Long> {
}
