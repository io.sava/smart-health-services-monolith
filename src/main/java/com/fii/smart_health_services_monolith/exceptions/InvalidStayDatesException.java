package com.fii.smart_health_services_monolith.exceptions;

public class InvalidStayDatesException extends RuntimeException {
    public InvalidStayDatesException() {
        super("Invalid dates. End date should be greater than start date");
    }
}
