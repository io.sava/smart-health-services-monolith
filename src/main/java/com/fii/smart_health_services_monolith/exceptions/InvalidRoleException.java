package com.fii.smart_health_services_monolith.exceptions;

public class InvalidRoleException extends RuntimeException {
    public InvalidRoleException(String role) {
        super(String.format("Invalid role: %s. Available roles: PATIENT, NURSE, DOCTOR, ADMIN", role));
    }
}
