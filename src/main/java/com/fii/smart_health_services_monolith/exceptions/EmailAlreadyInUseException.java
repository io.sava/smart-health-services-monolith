package com.fii.smart_health_services_monolith.exceptions;

public class EmailAlreadyInUseException extends RuntimeException {
    public EmailAlreadyInUseException(String email) {
        super(String.format("Email %s is already in use", email));
    }
}
