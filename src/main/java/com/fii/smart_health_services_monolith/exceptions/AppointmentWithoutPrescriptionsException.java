package com.fii.smart_health_services_monolith.exceptions;

public class AppointmentWithoutPrescriptionsException extends RuntimeException {
    public AppointmentWithoutPrescriptionsException(Long appointmentId) {
        super(String.format("Appointment with id %d has no prescriptions", appointmentId));
    }
}
