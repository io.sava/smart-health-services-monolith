package com.fii.smart_health_services_monolith.controllers;

import com.fii.smart_health_services_monolith.dtos.DoctorDto;
import com.fii.smart_health_services_monolith.entities.Doctor;
import com.fii.smart_health_services_monolith.services.DoctorService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("api/v1/doctors")
@RequiredArgsConstructor
public class DoctorController {
    private final DoctorService doctorService;

    @GetMapping
    public Page<Doctor> getAll(Pageable pageable) {
        return doctorService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public Doctor getById(@PathVariable @Valid @Min(0) Long id) {
        return doctorService.getById(id);
    }

    @GetMapping("/departments/{departmentId}")
    public List<Doctor> getByDepartmentId(@PathVariable @Valid @Min(0) Long departmentId) {
        return doctorService.getByDepartmentId(departmentId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid DoctorDto doctorDto) {
        doctorService.create(doctorDto);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable @Valid @Min(0) Long id, @RequestBody @Valid DoctorDto doctorDto) {
        doctorService.update(id, doctorDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable @Valid @Min(0) Long id) {
        doctorService.remove(id);
    }
}