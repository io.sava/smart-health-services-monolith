package com.fii.smart_health_services_monolith.controllers;

import com.fii.smart_health_services_monolith.dtos.StayDto;
import com.fii.smart_health_services_monolith.entities.Stay;
import com.fii.smart_health_services_monolith.services.StayService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("api/v1/stays")
@RequiredArgsConstructor
public class StayController {
    private final StayService stayService;

    @GetMapping("/{id}")
    @PreAuthorize("@stayService.canUserGetStay(principal, #id)")
    public Stay getById(@PathVariable @Valid @Min(0) Long id) {
        return stayService.getById(id);
    }

    @GetMapping("/patients/{patientId}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('DOCTOR') or hasAuthority('NURSE') or " +
            "(hasAuthority('PATIENT') and principal.ownerId == #patientId)")
    public List<Stay> getByPatientId(@PathVariable @Valid @Min(0) Long patientId) {
        return stayService.getByPatientId(patientId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("@stayService.canUserCreateStay(principal, #stayDto)")
    public void create(@RequestBody @Valid StayDto stayDto) {
        stayService.create(stayDto);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("@stayService.canUserUpdateStay(principal, #id, #stayDto)")
    public void update(@PathVariable @Valid @Min(0) Long id, @RequestBody @Valid StayDto stayDto) {
        stayService.update(id, stayDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable @Valid @Min(0) Long id) {
        stayService.remove(id);
    }
}
