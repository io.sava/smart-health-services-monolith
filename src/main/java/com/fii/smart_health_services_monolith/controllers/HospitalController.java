package com.fii.smart_health_services_monolith.controllers;

import com.fii.smart_health_services_monolith.dtos.HospitalDto;
import com.fii.smart_health_services_monolith.entities.Hospital;
import com.fii.smart_health_services_monolith.services.HospitalService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("api/v1/hospitals")
@RequiredArgsConstructor
public class HospitalController {
    private final HospitalService hospitalService;

    @GetMapping
    public List<Hospital> getAll() {
        return hospitalService.getAll();
    }

    @GetMapping("/{id}")
    public Hospital getById(@PathVariable @Valid @Min(0) Long id) {
        return hospitalService.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody @Valid HospitalDto hospitalDto) {
        hospitalService.create(hospitalDto);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(@PathVariable @Valid @Min(0) Long id, @RequestBody @Valid HospitalDto hospitalDto) {
        hospitalService.update(id, hospitalDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable @Valid @Min(0) Long id) {
        hospitalService.remove(id);
    }
}