package com.fii.smart_health_services_monolith.controllers;

import com.fii.smart_health_services_monolith.dtos.AppointmentDto;
import com.fii.smart_health_services_monolith.entities.Appointment;
import com.fii.smart_health_services_monolith.services.AppointmentService;
import com.lowagie.text.DocumentException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("api/v1/appointments")
@RequiredArgsConstructor
public class AppointmentController {
    private final AppointmentService appointmentService;

    @GetMapping("/{id}")
    @PreAuthorize("@appointmentService.canUserGetAppointment(principal, #id)")
    public Appointment getById(@PathVariable @Valid @Min(0) Long id) {
        return appointmentService.getById(id);
    }

    @GetMapping("/patients/{patientId}")
    @PreAuthorize("hasAuthority('ADMIN') or hasAuthority('DOCTOR') or hasAuthority('NURSE') or " +
            "(hasAuthority('PATIENT') and principal.ownerId == #patientId)")
    public List<Appointment> getByPatientId(@PathVariable @Valid @Min(0) Long patientId) {
        return appointmentService.getByPatientId(patientId);
    }

    @GetMapping("/{id}/prescriptions/export")
    @PreAuthorize("@appointmentService.canUserGetAppointment(principal, #id)")
    public void exportPrescriptionsToPDF(@PathVariable @Valid @Min(0) Long id,
                                         HttpServletResponse response) throws DocumentException, IOException {
        response.setContentType("application/pdf");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Reteta.pdf";
        response.setHeader(headerKey, headerValue);
        appointmentService.exportPrescriptions(id, response);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("@appointmentService.canUserCreateAppointment(principal, #appointmentDto)")
    public void create(@RequestBody @Valid AppointmentDto appointmentDto) {
        appointmentService.create(appointmentDto);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("@appointmentService.canUserUpdateAppointment(principal, #id, #appointmentDto)")
    public void update(@PathVariable @Valid @Min(0) Long id, @RequestBody @Valid AppointmentDto appointmentDto) {
        appointmentService.update(id, appointmentDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("@appointmentService.canUserDeleteAppointment(principal, #id)")
    public void remove(@PathVariable @Valid @Min(0) Long id) {
        appointmentService.remove(id);
    }
}