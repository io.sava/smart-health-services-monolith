package com.fii.smart_health_services_monolith.controllers;

import com.fii.smart_health_services_monolith.dtos.PrescriptionDto;
import com.fii.smart_health_services_monolith.entities.Prescription;
import com.fii.smart_health_services_monolith.services.PrescriptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("api/v1/prescriptions")
@RequiredArgsConstructor
public class PrescriptionController {
    private final PrescriptionService prescriptionService;

    @GetMapping("/appointments/{appointmentId}")
    @PreAuthorize("@appointmentService.canUserGetAppointment(principal, #appointmentId)")
    public List<Prescription> getByAppointmentId(@PathVariable @Valid @Min(0) Long appointmentId) {
        return prescriptionService.getByAppointmentId(appointmentId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("@prescriptionService.canUserCreatePrescription(principal, #prescriptionDto)")
    public void create(@RequestBody @Valid PrescriptionDto prescriptionDto) {
        prescriptionService.create(prescriptionDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("@prescriptionService.canUserDeletePrescription(principal, #id)")
    public void remove(@PathVariable @Valid @Min(0) Long id) {
        prescriptionService.remove(id);
    }
}