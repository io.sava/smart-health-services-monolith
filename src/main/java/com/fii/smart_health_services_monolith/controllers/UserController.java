package com.fii.smart_health_services_monolith.controllers;

import com.fii.smart_health_services_monolith.dtos.users.UserLoginDto;
import com.fii.smart_health_services_monolith.dtos.users.UserLoginResultDto;
import com.fii.smart_health_services_monolith.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("/login")
    public UserLoginResultDto login(@RequestBody @Valid UserLoginDto userLoginDto) {
        return userService.login(userLoginDto);
    }
}
