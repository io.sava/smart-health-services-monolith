package com.fii.smart_health_services_monolith.controllers;

import com.fii.smart_health_services_monolith.dtos.UndergoDto;
import com.fii.smart_health_services_monolith.entities.Undergo;
import com.fii.smart_health_services_monolith.services.UndergoService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("api/v1/undergoes")
@RequiredArgsConstructor
public class UndergoController {
    private final UndergoService undergoService;

    @GetMapping("/stays/{stayId}")
    @PreAuthorize("@stayService.canUserGetStay(principal, #stayId)")
    public List<Undergo> getByStayId(@PathVariable @Valid @Min(0) Long stayId) {
        return undergoService.getByStayId(stayId);
    }

    @GetMapping("/doctors")
    @PreAuthorize("!hasAuthority('PATIENT')")
    public Page<Undergo> getByDoctorId(@RequestParam Long doctorId, Pageable pageable) {
        return undergoService.getByDoctorId(doctorId, pageable);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("@undergoService.canUserCreateUndergo(principal, #undergoDto)")
    public void create(@RequestBody @Valid UndergoDto undergoDto) {
        undergoService.create(undergoDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("@undergoService.canUserDeleteUndergo(principal, #id)")
    public void remove(@PathVariable @Valid @Min(0) Long id) {
        undergoService.remove(id);
    }
}
