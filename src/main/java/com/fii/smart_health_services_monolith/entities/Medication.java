package com.fii.smart_health_services_monolith.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@Table(name = "medications")
public class Medication extends BaseEntity {
    private String name;
    private String brand;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "medication")
    private List<Prescription> prescriptions;

    public Medication() {
        this.prescriptions = new ArrayList<>();
    }
}
