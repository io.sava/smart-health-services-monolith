package com.fii.smart_health_services_monolith.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@Table(name = "patients")
public class Patient extends BaseEntity {
    @Column(unique = true)
    private String cnp;

    private String name;
    private Integer age;
    private String address;
    private String phone;
    private String email;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patient")
    private List<Appointment> appointments;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "patient")
    private List<Stay> stays;

    public Patient() {
        this.appointments = new ArrayList<>();
        this.stays = new ArrayList<>();
    }
}
