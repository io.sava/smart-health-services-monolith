package com.fii.smart_health_services_monolith.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@Table(name = "procedures")
public class Procedure extends BaseEntity {
    private String name;
    private Float cost;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "procedure")
    private List<Undergo> undergoes;

    public Procedure() {
        this.undergoes = new ArrayList<>();
    }
}
