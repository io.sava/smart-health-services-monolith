package com.fii.smart_health_services_monolith.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountDetails {
    private Long ownerId;
    private String email;
    private Role role;
}
