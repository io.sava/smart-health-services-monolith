package com.fii.smart_health_services_monolith.entities;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    ADMIN, DOCTOR, PATIENT, NURSE;

    public String getAuthority() {
        return name();
    }
}
