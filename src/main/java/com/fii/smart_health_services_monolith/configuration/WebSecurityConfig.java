package com.fii.smart_health_services_monolith.configuration;

import com.fii.smart_health_services_monolith.entities.Role;
import com.fii.smart_health_services_monolith.security.jwt.JwtTokenFilterConfigurer;
import com.fii.smart_health_services_monolith.security.jwt.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors();
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/api/v1/appointments/**").authenticated()
                .antMatchers(HttpMethod.POST, "/api/v1/appointments").hasAnyAuthority(Role.NURSE.getAuthority())
                .antMatchers(HttpMethod.PUT, "/api/v1/appointments/**").hasAnyAuthority(Role.NURSE.getAuthority())
                .antMatchers(HttpMethod.DELETE, "/api/v1/appointments/**").hasAnyAuthority(Role.NURSE.getAuthority(), Role.ADMIN.getAuthority())

                .antMatchers(HttpMethod.GET, "/api/v1/departments").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/departments/**").permitAll()

                .antMatchers(HttpMethod.GET, "/api/v1/doctors").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/doctors/**").permitAll()

                .antMatchers(HttpMethod.GET, "/api/v1/hospitals").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/hospitals/**").permitAll()

                .antMatchers(HttpMethod.GET, "/api/v1/medications").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/medications/**").permitAll()

                .antMatchers(HttpMethod.GET, "/api/v1/nurses").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/nurses/**").permitAll()

                .antMatchers(HttpMethod.GET, "/api/v1/patients").hasAnyAuthority(Role.DOCTOR.getAuthority(), Role.NURSE.getAuthority(), Role.ADMIN.getAuthority())
                .antMatchers(HttpMethod.GET, "/api/v1/patients/**").authenticated()
                .antMatchers(HttpMethod.POST, "/api/v1/patients").hasAnyAuthority(Role.NURSE.getAuthority(), Role.ADMIN.getAuthority())
                .antMatchers(HttpMethod.PUT, "/api/v1/patients/**").hasAnyAuthority(Role.NURSE.getAuthority(), Role.ADMIN.getAuthority())

                .antMatchers(HttpMethod.GET, "/api/v1/prescriptions/**").authenticated()
                .antMatchers(HttpMethod.POST, "/api/v1/prescriptions").hasAnyAuthority(Role.DOCTOR.getAuthority(), Role.NURSE.getAuthority())
                .antMatchers(HttpMethod.DELETE, "/api/v1/prescriptions/**").hasAnyAuthority(Role.DOCTOR.getAuthority(), Role.NURSE.getAuthority())

                .antMatchers(HttpMethod.GET, "/api/v1/procedures").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/procedures/**").permitAll()

                .antMatchers(HttpMethod.GET, "/api/v1/rooms/**").permitAll()

                .antMatchers(HttpMethod.GET, "/api/v1/stays/**").authenticated()
                .antMatchers(HttpMethod.POST, "/api/v1/stays").hasAnyAuthority(Role.DOCTOR.getAuthority(), Role.NURSE.getAuthority())
                .antMatchers(HttpMethod.PUT, "/api/v1/stays/**").hasAnyAuthority(Role.NURSE.getAuthority(), Role.DOCTOR.getAuthority())

                .antMatchers(HttpMethod.GET, "/api/v1/undergoes/**").authenticated()
                .antMatchers(HttpMethod.POST, "/api/v1/undergoes").hasAnyAuthority(Role.DOCTOR.getAuthority())
                .antMatchers(HttpMethod.DELETE, "/api/v1/undergoes/**").hasAnyAuthority(Role.DOCTOR.getAuthority(), Role.ADMIN.getAuthority())

                .antMatchers(HttpMethod.POST, "/api/v1/users/**").permitAll()

                .anyRequest().hasAuthority(Role.ADMIN.getAuthority());

        http.apply(new JwtTokenFilterConfigurer(jwtTokenProvider));
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
