package com.fii.smart_health_services_monolith.dtos.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRegisterDto {
    @Email
    @NotNull(message = "Email cannot be null")
    private String email;

    @Size(min = 8, message = "Minimum password length: 8 characters")
    @NotNull(message = "Password cannot be null")
    private String password;

    @NotNull(message = "Role cannot be null")
    private String role;

    private Long ownerId;
}
