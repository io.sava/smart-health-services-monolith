package com.fii.smart_health_services_monolith.dtos.users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserLoginResultDto {
    private Long ownerId;
    private String role;
    private String token;
}
