package com.fii.smart_health_services_monolith.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PatientDto {
    @NotNull(message = "Cnp cannot be null")
    @Pattern(regexp = "\\b[1-8]\\d{2}(0[1-9]|1[0-2])(0[1-9]|[12]\\d|3[01])(0[1-9]|[1-4]\\d|5[0-2]|99)\\d{4}\\b", message = "Invalid cnp")
    private String cnp;

    @NotNull(message = "Name cannot be null")
    private String name;

    @NotNull(message = "Age cannot be null")
    @Min(value = 0, message = "The age must be positive")
    private Integer age;

    private String address;
    private String phone;

    @Email
    @NotNull(message = "Email cannot be null")
    private String email;
}
