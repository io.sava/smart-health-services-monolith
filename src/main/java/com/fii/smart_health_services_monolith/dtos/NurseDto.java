package com.fii.smart_health_services_monolith.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NurseDto {
    @NotNull(message = "Name cannot be null")
    private String name;

    @NotNull(message = "Position cannot be null")
    private String position;

    @Email
    @NotNull(message = "Email cannot be null")
    private String email;

    @NotNull(message = "DepartmentId cannot be null")
    private Long departmentId;
}
