package com.fii.smart_health_services_monolith.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UndergoDto {
    @NotNull(message = "ProcedureId cannot be null")
    private Long procedureId;

    @NotNull(message = "StayId cannot be null")
    private Long stayId;

    @NotNull(message = "DoctorId cannot be null")
    private Long doctorId;
}
