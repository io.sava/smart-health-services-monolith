package com.fii.smart_health_services_monolith.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProcedureDto {
    @NotNull(message = "Name cannot be null")
    private String name;

    @NotNull(message = "Cost cannot be null")
    private Float cost;
}
