package com.fii.smart_health_services_monolith.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentDto {
    @NotNull(message = "Date cannot be null")
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private LocalDateTime date;

    private String diagnosis;

    @NotNull(message = "PatientId cannot be null")
    private Long patientId;

    @NotNull(message = "NurseId cannot be null")
    private Long nurseId;

    @NotNull(message = "DoctorId cannot be null")
    private Long doctorId;
}
