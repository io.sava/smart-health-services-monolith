package com.fii.smart_health_services_monolith.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HospitalDto {
    @NotNull(message = "Name cannot be null")
    private String name;

    @NotNull(message = "Address cannot be null")
    private String address;

    private String fax;
    private String phone;

    @NotNull(message = "Email cannot be null")
    @Email(message = "Email should be valid")
    private String email;
}
