package com.fii.smart_health_services_monolith.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentDto {
    @NotNull(message = "Name cannot be null")
    private String name;

    @NotNull(message = "HospitalId cannot be null")
    private Long hospitalId;
}
