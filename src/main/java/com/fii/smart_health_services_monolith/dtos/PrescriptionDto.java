package com.fii.smart_health_services_monolith.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PrescriptionDto {
    @NotNull(message = "Dose cannot be null")
    private String dose;

    @NotNull(message = "MedicationId cannot be null")
    private Long medicationId;

    @NotNull(message = "AppointmentId cannot be null")
    private Long appointmentId;
}
