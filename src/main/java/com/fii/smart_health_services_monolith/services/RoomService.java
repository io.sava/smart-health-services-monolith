package com.fii.smart_health_services_monolith.services;

import com.fii.smart_health_services_monolith.dtos.RoomDto;
import com.fii.smart_health_services_monolith.entities.Room;
import com.fii.smart_health_services_monolith.exceptions.EntityNotFoundException;
import com.fii.smart_health_services_monolith.mappers.RoomMapper;
import com.fii.smart_health_services_monolith.repositories.RoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class RoomService {
    private final RoomRepository roomRepository;
    private final RoomMapper roomMapper;
    private final HospitalService hospitalService;

    public Room getById(Long id) {
        return roomRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Room", id));
    }

    public List<Room> getByHospitalId(Long hospitalId) {
        hospitalService.getById(hospitalId);

        return roomRepository.findAll().stream()
                .filter(room -> room.getHospital().getId().equals(hospitalId))
                .collect(Collectors.toList());
    }

    public void create(RoomDto roomDto) {
        roomRepository.save(roomMapper.toRoom(roomDto));
    }

    public void update(Long id, RoomDto roomDto) {
        this.getById(id);
        Room roomToUpdate = roomMapper.toRoom(roomDto);
        roomToUpdate.setId(id);
        roomRepository.save(roomToUpdate);
    }

    public void remove(Long id) {
        Room room = this.getById(id);
        roomRepository.delete(room);
    }
}
