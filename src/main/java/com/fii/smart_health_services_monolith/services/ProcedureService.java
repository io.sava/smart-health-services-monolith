package com.fii.smart_health_services_monolith.services;

import com.fii.smart_health_services_monolith.dtos.ProcedureDto;
import com.fii.smart_health_services_monolith.entities.Procedure;
import com.fii.smart_health_services_monolith.exceptions.EntityNotFoundException;
import com.fii.smart_health_services_monolith.mappers.ProcedureMapper;
import com.fii.smart_health_services_monolith.repositories.ProcedureRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class ProcedureService {
    private final ProcedureRepository procedureRepository;
    private final ProcedureMapper procedureMapper;

    public List<Procedure> getAll() {
        return procedureRepository.findAll();
    }

    public Procedure getById(Long id) {
        return procedureRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Procedure", id));
    }

    public void create(ProcedureDto procedureDto) {
        procedureRepository.save(procedureMapper.toProcedure(procedureDto));
    }

    public void update(Long id, ProcedureDto procedureDto) {
        this.getById(id);
        Procedure procedureToUpdate = procedureMapper.toProcedure(procedureDto);
        procedureToUpdate.setId(id);
        procedureRepository.save(procedureToUpdate);
    }

    public void remove(Long id) {
        Procedure procedure = this.getById(id);
        procedureRepository.delete(procedure);
    }
}
