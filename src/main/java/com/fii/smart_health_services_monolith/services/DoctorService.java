package com.fii.smart_health_services_monolith.services;

import com.fii.smart_health_services_monolith.dtos.DoctorDto;
import com.fii.smart_health_services_monolith.entities.AccountDetails;
import com.fii.smart_health_services_monolith.entities.Doctor;
import com.fii.smart_health_services_monolith.entities.Role;
import com.fii.smart_health_services_monolith.entities.User;
import com.fii.smart_health_services_monolith.exceptions.EntityNotFoundException;
import com.fii.smart_health_services_monolith.mappers.DoctorMapper;
import com.fii.smart_health_services_monolith.repositories.DoctorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class DoctorService {
    private static final String DOCTOR_ENTITY = "Doctor";
    private final DoctorRepository doctorRepository;
    private final DoctorMapper doctorMapper;
    private final UserService userService;
    private final DepartmentService departmentService;

    public Page<Doctor> getAll(Pageable pageable) {
        return doctorRepository.findAll(pageable);
    }

    public Doctor getById(Long id) {
        return doctorRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(DOCTOR_ENTITY, id));
    }

    public List<Doctor> getByDepartmentId(Long departmentId) {
        departmentService.getById(departmentId);

        return doctorRepository.findAll().stream()
                .filter(doctor -> doctor.getDepartment().getId().equals(departmentId))
                .collect(Collectors.toList());
    }

    public void create(DoctorDto doctorDto) {
        departmentService.checkIfExists(doctorDto.getDepartmentId());
        Doctor doctor = doctorRepository.save(doctorMapper.toDoctor(doctorDto));
        userService.generateAccount(getAccountDetails(doctor));
    }

    public void update(Long id, DoctorDto doctorDto) {
        this.checkIfExists(id);
        departmentService.checkIfExists(doctorDto.getDepartmentId());
        Doctor doctorToUpdate = doctorMapper.toDoctor(doctorDto);
        doctorToUpdate.setId(id);
        doctorRepository.save(doctorToUpdate);
        User user = userService.getByOwnerIdAndRole(id, Role.DOCTOR);
        userService.updateEmail(user.getId(), doctorDto.getEmail());
    }

    public void remove(Long id) {
        Doctor doctor = getById(id);
        doctorRepository.delete(doctor);
        userService.remove(doctor.getEmail());
    }

    public void checkIfExists(Long id) {
        if (doctorRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException(DOCTOR_ENTITY, id);
        }
    }

    private AccountDetails getAccountDetails(Doctor doctor) {
        return new AccountDetails(doctor.getId(), doctor.getEmail(), Role.DOCTOR);
    }
}
