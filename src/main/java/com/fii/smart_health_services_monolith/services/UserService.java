package com.fii.smart_health_services_monolith.services;

import com.fii.smart_health_services_monolith.dtos.users.UserLoginDto;
import com.fii.smart_health_services_monolith.dtos.users.UserLoginResultDto;
import com.fii.smart_health_services_monolith.dtos.users.UserRegisterDto;
import com.fii.smart_health_services_monolith.entities.AccountDetails;
import com.fii.smart_health_services_monolith.entities.Role;
import com.fii.smart_health_services_monolith.entities.User;
import com.fii.smart_health_services_monolith.exceptions.EmailAlreadyInUseException;
import com.fii.smart_health_services_monolith.exceptions.EntityNotFoundException;
import com.fii.smart_health_services_monolith.mappers.UserMapper;
import com.fii.smart_health_services_monolith.repositories.UserRepository;
import com.fii.smart_health_services_monolith.security.jwt.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordGeneratorService passwordGeneratorService;
    private final MailSenderService mailSenderService;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;

    @Value("${account.mail.subject}")
    private String emailSubject;

    @Value("${account.mail.text}")
    private String emailText;

    public User getByOwnerIdAndRole(Long ownerId, Role role) {
        return userRepository.findByOwnerIdAndRole(ownerId, role).orElseThrow(() -> new EntityNotFoundException("User"));
    }

    public void generateAccount(AccountDetails accountDetails) {
        String password = passwordGeneratorService.generatePassword();
        UserRegisterDto newUser =
                new UserRegisterDto(accountDetails.getEmail(), password, accountDetails.getRole().getAuthority(), accountDetails.getOwnerId());
        register(newUser);
        mailSenderService.sendEmail(accountDetails.getEmail(), emailSubject,
                String.format(emailText, accountDetails.getEmail(), password));
    }

    public UserLoginResultDto login(UserLoginDto userLoginDto) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(userLoginDto.getEmail(), userLoginDto.getPassword()));

        User user = getUserByEmail(userLoginDto.getEmail());
        String token = jwtTokenProvider.createToken(user.getEmail(), user.getRole());
        return new UserLoginResultDto(user.getOwnerId(), user.getRole().toString(), token);
    }

    public void register(UserRegisterDto userRegisterDto) {
        if (userRepository.existsByEmail(userRegisterDto.getEmail())) {
            throw new EmailAlreadyInUseException(userRegisterDto.getEmail());
        }
        User user = userMapper.toUser(userRegisterDto);
        userRepository.save(user);
    }

    public void updateEmail(Long userId, String email) {
        checkIfExists(userId);
        userRepository.updateEmail(userId, email);
    }

    public void remove(String email) {
        getUserByEmail(email);
        userRepository.deleteByEmail(email);
    }

    private void checkIfExists(Long userId) {
        if (userRepository.findById(userId).isEmpty()) {
            throw new EntityNotFoundException("User", userId);
        }
    }

    private User getUserByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(() -> new EntityNotFoundException("User"));
    }
}
