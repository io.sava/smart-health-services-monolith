package com.fii.smart_health_services_monolith.services;

import com.fii.smart_health_services_monolith.dtos.DepartmentDto;
import com.fii.smart_health_services_monolith.entities.Department;
import com.fii.smart_health_services_monolith.exceptions.EntityNotFoundException;
import com.fii.smart_health_services_monolith.mappers.DepartmentMapper;
import com.fii.smart_health_services_monolith.repositories.DepartmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class DepartmentService {
    private final DepartmentRepository departmentRepository;
    private final DepartmentMapper departmentMapper;
    private final HospitalService hospitalService;

    public Department getById(Long id) {
        return departmentRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Department", id));
    }

    public List<Department> getByHospitalId(Long hospitalId) {
        hospitalService.getById(hospitalId);

        return departmentRepository.findAll().stream()
                .filter(department -> department.getHospital().getId().equals(hospitalId))
                .collect(Collectors.toList());
    }

    public void create(DepartmentDto departmentDto) {
        hospitalService.checkIfExists(departmentDto.getHospitalId());
        departmentRepository.save(departmentMapper.toDepartment(departmentDto));
    }

    public void update(Long id, DepartmentDto departmentDto) {
        this.getById(id);
        hospitalService.checkIfExists(departmentDto.getHospitalId());
        Department departmentToUpdate = departmentMapper.toDepartment(departmentDto);
        departmentToUpdate.setId(id);
        departmentRepository.save(departmentToUpdate);
    }

    public void remove(Long id) {
        Department department = this.getById(id);
        departmentRepository.delete(department);
    }

    public void checkIfExists(Long id) {
        if (departmentRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException("Department", id);
        }
    }
}
