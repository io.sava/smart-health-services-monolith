package com.fii.smart_health_services_monolith.services;

import com.fii.smart_health_services_monolith.dtos.HospitalDto;
import com.fii.smart_health_services_monolith.entities.Hospital;
import com.fii.smart_health_services_monolith.exceptions.EntityNotFoundException;
import com.fii.smart_health_services_monolith.mappers.HospitalMapper;
import com.fii.smart_health_services_monolith.repositories.DepartmentRepository;
import com.fii.smart_health_services_monolith.repositories.HospitalRepository;
import com.fii.smart_health_services_monolith.repositories.RoomRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class HospitalService {
    private final HospitalRepository hospitalRepository;
    private final DepartmentRepository departmentRepository;
    private final RoomRepository roomRepository;
    private final HospitalMapper hospitalMapper;

    public List<Hospital> getAll() {
        return new ArrayList<>(hospitalRepository.findAll());
    }

    public Hospital getById(Long id) {
        return hospitalRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Hospital", id));
    }

    public void create(HospitalDto hospitalDto) {
        hospitalRepository.save(hospitalMapper.toHospital(hospitalDto));
    }

    public void update(Long id, HospitalDto hospitalDto) {
        this.getById(id);
        Hospital hospitalToUpdate = hospitalMapper.toHospital(hospitalDto);
        hospitalToUpdate.setId(id);
        hospitalRepository.save(hospitalToUpdate);
    }

    public void remove(Long id) {
        Hospital hospital = this.getById(id);
        hospital.getDepartments().forEach(department -> departmentRepository.deleteById(department.getId()));
        hospital.getRooms().forEach(room -> roomRepository.deleteById(room.getId()));
        hospitalRepository.delete(hospital);
    }

    public void checkIfExists(Long id) {
        if (hospitalRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException("Hospital", id);
        }
    }
}
