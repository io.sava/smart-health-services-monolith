package com.fii.smart_health_services_monolith.services;

import com.fii.smart_health_services_monolith.dtos.MedicationDto;
import com.fii.smart_health_services_monolith.entities.Medication;
import com.fii.smart_health_services_monolith.exceptions.EntityNotFoundException;
import com.fii.smart_health_services_monolith.mappers.MedicationMapper;
import com.fii.smart_health_services_monolith.repositories.MedicationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class MedicationService {
    private final MedicationRepository medicationRepository;
    private final MedicationMapper medicationMapper;

    public List<Medication> getAll() {
        return medicationRepository.findAll();
    }

    public Medication getById(Long id) {
        return medicationRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Medication", id));
    }

    public void create(MedicationDto medicationDto) {
        medicationRepository.save(medicationMapper.toMedication(medicationDto));
    }

    public void remove(Long id) {
        Medication medication = this.getById(id);
        medicationRepository.delete(medication);
    }
}
