package com.fii.smart_health_services_monolith.services;

import com.fii.smart_health_services_monolith.dtos.NurseDto;
import com.fii.smart_health_services_monolith.entities.AccountDetails;
import com.fii.smart_health_services_monolith.entities.Nurse;
import com.fii.smart_health_services_monolith.entities.Role;
import com.fii.smart_health_services_monolith.entities.User;
import com.fii.smart_health_services_monolith.exceptions.EntityNotFoundException;
import com.fii.smart_health_services_monolith.mappers.NurseMapper;
import com.fii.smart_health_services_monolith.repositories.NurseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class NurseService {
    private static final String NURSE_ENTITY = "Nurse";
    private final NurseRepository nurseRepository;
    private final NurseMapper nurseMapper;
    private final UserService userService;
    private final DepartmentService departmentService;

    public Page<Nurse> getAll(Pageable pageable) {
        return nurseRepository.findAll(pageable);
    }

    public Nurse getById(Long id) {
        return nurseRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(NURSE_ENTITY, id));
    }

    public List<Nurse> getByDepartmentId(Long departmentId) {
        departmentService.getById(departmentId);

        return nurseRepository.findAll().stream()
                .filter(nurse -> nurse.getDepartment().getId().equals(departmentId))
                .collect(Collectors.toList());
    }

    public void create(NurseDto nurseDto) {
        departmentService.checkIfExists(nurseDto.getDepartmentId());
        Nurse nurse = nurseRepository.save(nurseMapper.toNurse(nurseDto));
        userService.generateAccount(getAccountDetails(nurse));
    }

    public void update(Long id, NurseDto nurseDto) {
        this.checkIfExists(id);
        departmentService.checkIfExists(nurseDto.getDepartmentId());
        Nurse nurseToUpdate = nurseMapper.toNurse(nurseDto);
        nurseToUpdate.setId(id);
        nurseRepository.save(nurseToUpdate);
        User user = userService.getByOwnerIdAndRole(id, Role.NURSE);
        userService.updateEmail(user.getId(), nurseDto.getEmail());
    }

    public void remove(Long id) {
        Nurse nurse = getById(id);
        nurseRepository.delete(nurse);
        userService.remove(nurse.getEmail());
    }

    private AccountDetails getAccountDetails(Nurse nurse) {
        return new AccountDetails(nurse.getId(), nurse.getEmail(), Role.NURSE);
    }

    private void checkIfExists(Long id) {
        if (nurseRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException(NURSE_ENTITY, id);
        }
    }
}
