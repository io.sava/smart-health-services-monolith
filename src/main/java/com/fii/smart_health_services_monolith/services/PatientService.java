package com.fii.smart_health_services_monolith.services;

import com.fii.smart_health_services_monolith.dtos.PatientDto;
import com.fii.smart_health_services_monolith.entities.AccountDetails;
import com.fii.smart_health_services_monolith.entities.Patient;
import com.fii.smart_health_services_monolith.entities.Role;
import com.fii.smart_health_services_monolith.entities.User;
import com.fii.smart_health_services_monolith.exceptions.EntityNotFoundException;
import com.fii.smart_health_services_monolith.mappers.PatientMapper;
import com.fii.smart_health_services_monolith.repositories.PatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class PatientService {
    private final PatientRepository patientRepository;
    private final PatientMapper patientMapper;
    private final UserService userService;

    public Page<Patient> getAll(Pageable pageable) {
        return patientRepository.findAll(pageable);
    }

    public Patient getById(Long id) {
        return patientRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Patient", id));
    }

    public void create(PatientDto patientDto) {
        Patient patient = patientRepository.save(patientMapper.toPatient(patientDto));
        userService.generateAccount(getAccountDetails(patient));
    }

    public void update(Long id, PatientDto patientDto) {
        getById(id);
        Patient patientToUpdate = patientMapper.toPatient(patientDto);
        patientToUpdate.setId(id);
        patientRepository.save(patientToUpdate);
        User user = userService.getByOwnerIdAndRole(id, Role.PATIENT);
        userService.updateEmail(user.getId(), patientDto.getEmail());
    }

    public void remove(Long id) {
        Patient patient = getById(id);
        patientRepository.delete(patient);
        userService.remove(patient.getEmail());
    }

    public void checkIfExists(Long id) {
        if (patientRepository.findById(id).isEmpty()) {
            throw new EntityNotFoundException("Patient", id);
        }
    }

    private AccountDetails getAccountDetails(Patient patient) {
        return new AccountDetails(patient.getId(), patient.getEmail(), Role.PATIENT);
    }
}
