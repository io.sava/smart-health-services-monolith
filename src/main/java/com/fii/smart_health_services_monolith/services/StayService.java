package com.fii.smart_health_services_monolith.services;

import com.fii.smart_health_services_monolith.dtos.StayDto;
import com.fii.smart_health_services_monolith.entities.Doctor;
import com.fii.smart_health_services_monolith.entities.Nurse;
import com.fii.smart_health_services_monolith.entities.Role;
import com.fii.smart_health_services_monolith.entities.Room;
import com.fii.smart_health_services_monolith.entities.Stay;
import com.fii.smart_health_services_monolith.entities.Undergo;
import com.fii.smart_health_services_monolith.entities.User;
import com.fii.smart_health_services_monolith.exceptions.EntityNotFoundException;
import com.fii.smart_health_services_monolith.exceptions.InvalidStayDatesException;
import com.fii.smart_health_services_monolith.mappers.StayMapper;
import com.fii.smart_health_services_monolith.repositories.StayRepository;
import com.fii.smart_health_services_monolith.repositories.UndergoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class StayService {
    private final StayRepository stayRepository;
    private final UndergoRepository undergoRepository;
    private final StayMapper stayMapper;
    private final PatientService patientService;
    private final RoomService roomService;
    private final DoctorService doctorService;
    private final NurseService nurseService;

    public Stay getById(Long id) {
        return stayRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Stay", id));
    }

    public List<Stay> getByPatientId(Long patientId) {
        patientService.checkIfExists(patientId);

        return stayRepository.findAll().stream()
                .filter(stay -> stay.getPatient().getId().equals(patientId))
                .collect(Collectors.toList());
    }

    public void create(StayDto stayDto) {
        if (!checkIfStayDatesAreValid(stayDto.getStartDate(), stayDto.getEndDate())) {
            throw new InvalidStayDatesException();
        }
        stayRepository.save(stayMapper.toStay(stayDto));
    }

    public void update(Long id, StayDto stayDto) {
        this.getById(id);
        if (!checkIfStayDatesAreValid(stayDto.getStartDate(), stayDto.getEndDate())) {
            throw new InvalidStayDatesException();
        }
        Stay stayToUpdate = stayMapper.toStay(stayDto);
        stayToUpdate.setId(id);
        stayRepository.save(stayToUpdate);
    }

    public void remove(Long id) {
        Stay stay = this.getById(id);
        for (Undergo undergo : stay.getUndergoes()) {
            undergoRepository.delete(undergo);
        }
        stayRepository.delete(stay);
    }

    public boolean canUserGetStay(User user, Long stayId) {
        if (user.getRole().equals(Role.PATIENT)) {
            Stay stay = this.getById(stayId);
            return user.getOwnerId().equals(stay.getPatient().getId());
        }
        return true;
    }

    public boolean canUserCreateStay(User user, StayDto stayDto) {
        Room room = this.roomService.getById(stayDto.getRoomId());
        if (user.getRole().equals(Role.DOCTOR)) {
            Doctor doctor = this.doctorService.getById(user.getOwnerId());
            return doctor.getDepartment().getHospital().getId().equals(room.getHospital().getId());
        }

        if (user.getRole().equals(Role.NURSE)) {
            Nurse nurse = this.nurseService.getById(user.getOwnerId());
            return nurse.getDepartment().getHospital().getId().equals(room.getHospital().getId());
        }

        return false;
    }

    public boolean canUserUpdateStay(User user, Long id, StayDto stayDto) {
        if (canUserCreateStay(user, stayDto)) {
            Stay stay = getById(id);

            if (user.getRole().equals(Role.DOCTOR)) {
                Doctor doctor = this.doctorService.getById(user.getOwnerId());
                return doctor.getDepartment().getHospital().getId().equals(stay.getRoom().getHospital().getId());
            }

            if (user.getRole().equals(Role.NURSE)) {
                Nurse nurse = this.nurseService.getById(user.getOwnerId());
                return nurse.getDepartment().getHospital().getId().equals(stay.getRoom().getHospital().getId());
            }
        }

        return false;
    }

    private boolean checkIfStayDatesAreValid(LocalDateTime startDate, LocalDateTime endDate) {
        return endDate.isAfter(startDate);
    }
}
