package com.fii.smart_health_services_monolith;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartHealthServicesMonolithApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmartHealthServicesMonolithApplication.class, args);
    }

}
