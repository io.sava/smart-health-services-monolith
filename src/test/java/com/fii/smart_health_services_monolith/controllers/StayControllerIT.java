package com.fii.smart_health_services_monolith.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fii.smart_health_services_monolith.config.MailSenderTestConfig;
import com.fii.smart_health_services_monolith.dtos.StayDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@RunWith(SpringRunner.class)
@Import(MailSenderTestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class StayControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    @WithUserDetails("admin@gmail.com")
    public void getStayWithId_1_shouldReturnOk_whenStayWithId_1_isInDatabase() throws Exception {
        Long stayId = 1L;

        this.mockMvc.perform(get("/api/v1/stays/{id}", stayId))
                .andExpect(status().isOk());
    }

    @Test
    @WithUserDetails("admin@gmail.com")
    public void getStayWithId_100_shouldReturnNotFound_whenStayWithId_100_isNotInDatabase() throws Exception {
        Long stayId = 100L;

        this.mockMvc.perform(get("/api/v1/stays/{id}", stayId))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void getStaysByPatient_shouldReturnOk_whenPatientWithId_1_isInDatabase() throws Exception {
        Long patientId = 1L;

        this.mockMvc.perform(get("/api/v1/stays/patients/{id}", patientId))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void getStaysByPatient_shouldReturnNotFound_whenPatientWithId_100_isNotInDatabase() throws Exception {
        Long patientId = 100L;

        this.mockMvc.perform(get("/api/v1/stays/patients/{id}", patientId))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails("virgil@gmail.com")
    public void createStay_shouldReturnCreated_whenStayIsValid() throws Exception {
        StayDto stay = new StayDto(LocalDateTime.now(), LocalDateTime.now().plusDays(5), 1L, 1L);

        this.mockMvc.perform(post("/api/v1/stays")
                .content(mapper.writeValueAsString(stay))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    @WithUserDetails("virgil@gmail.com")
    public void createStay_shouldReturnNotFound_whenPatientWithId_100_IsNotInDatabase() throws Exception {
        StayDto stay = new StayDto(LocalDateTime.now(), LocalDateTime.now().plusDays(5), 100L, 1L);

        this.mockMvc.perform(post("/api/v1/stays")
                .content(mapper.writeValueAsString(stay))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails("virgil@gmail.com")
    public void createStay_shouldReturnNotFound_whenRoomWithId_100_IsNotInDatabase() throws Exception {
        StayDto stay = new StayDto(LocalDateTime.now(), LocalDateTime.now().plusDays(5), 1L, 100L);

        this.mockMvc.perform(post("/api/v1/stays")
                .content(mapper.writeValueAsString(stay))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails("virgil@gmail.com")
    public void createStay_shouldReturnBadRequest_whenStartDateIsGreaterThanEndDate() throws Exception {
        StayDto stay = new StayDto(LocalDateTime.now().plusDays(5), LocalDateTime.now(), 1L, 1L);

        this.mockMvc.perform(post("/api/v1/stays")
                .content(mapper.writeValueAsString(stay))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    @WithUserDetails("virgil@gmail.com")
    public void updateStayWithId_1_shouldReturnNoContent_whenStayIsValid() throws Exception {
        Long stayId = 1L;
        StayDto stay = new StayDto(LocalDateTime.now(), LocalDateTime.now().plusDays(5), 1L, 1L);

        this.mockMvc.perform(put("/api/v1/stays/{id}", stayId)
                .content(mapper.writeValueAsString(stay))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithUserDetails("virgil@gmail.com")
    public void updateStayWithId_100_shouldReturnNotFound_whenStayWithId_100_IsNotInDatabase() throws Exception {
        Long stayId = 100L;
        StayDto stay = new StayDto(LocalDateTime.now(), LocalDateTime.now().plusDays(5), 1L, 1L);

        this.mockMvc.perform(put("/api/v1/stays/{id}", stayId)
                .content(mapper.writeValueAsString(stay))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails("virgil@gmail.com")
    public void updateStayWithId_1_shouldReturnNotFound_whenPatientWithId_100_IsNotInDatabase() throws Exception {
        Long stayId = 1L;
        StayDto stay = new StayDto(LocalDateTime.now(), LocalDateTime.now().plusDays(5), 100L, 1L);

        this.mockMvc.perform(put("/api/v1/stays/{id}", stayId)
                .content(mapper.writeValueAsString(stay))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails("virgil@gmail.com")
    public void updateStayWithId_1_shouldReturnNotFound_whenRoomWithId_100_IsNotInDatabase() throws Exception {
        Long stayId = 1L;
        StayDto stay = new StayDto(LocalDateTime.now(), LocalDateTime.now().plusDays(5), 1L, 100L);

        this.mockMvc.perform(put("/api/v1/stays/{id}", stayId)
                .content(mapper.writeValueAsString(stay))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails("virgil@gmail.com")
    public void updateStayWithId_1_shouldReturnBadRequest_whenStartDateIsGreaterThanEndDate() throws Exception {
        Long stayId = 1L;
        StayDto stay = new StayDto(LocalDateTime.now().plusDays(5), LocalDateTime.now(), 1L, 1L);

        this.mockMvc.perform(put("/api/v1/stays/{id}", stayId)
                .content(mapper.writeValueAsString(stay))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteStayWithId_1_shouldReturnNoContent_whenStayWithId_1_isInDatabase() throws Exception {
        Long stayId = 1L;

        this.mockMvc.perform(delete("/api/v1/stays/{id}", stayId))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteStayWithId_100_shouldReturnNotFound_whenStayWithId_100_isNotInDatabase() throws Exception {
        Long stayId = 100L;

        this.mockMvc.perform(delete("/api/v1/stays/{id}", stayId))
                .andExpect(status().isNotFound());
    }
}
