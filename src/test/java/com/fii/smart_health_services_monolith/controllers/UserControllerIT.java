package com.fii.smart_health_services_monolith.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fii.smart_health_services_monolith.config.MailSenderTestConfig;
import com.fii.smart_health_services_monolith.dtos.users.UserLoginDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@RunWith(SpringRunner.class)
@Import(MailSenderTestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void login_shouldReturnOk_whenUserIsInDatabase() throws Exception {
        UserLoginDto userLoginDto = new UserLoginDto("admin@gmail.com", "admin@gmail.com");

        this.mockMvc.perform(post("/api/v1/users/login")
                .content(mapper.writeValueAsString(userLoginDto))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
