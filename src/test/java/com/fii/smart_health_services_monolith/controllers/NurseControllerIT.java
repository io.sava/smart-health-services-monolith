package com.fii.smart_health_services_monolith.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fii.smart_health_services_monolith.config.MailSenderTestConfig;
import com.fii.smart_health_services_monolith.dtos.NurseDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@RunWith(SpringRunner.class)
@Import(MailSenderTestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class NurseControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void getNurseWithId_1_shouldReturnOk_whenNurseWithId_1_isInDatabase() throws Exception {
        Long nurseId = 1L;

        this.mockMvc.perform(get("/api/v1/nurses/{id}", nurseId))
                .andExpect(status().isOk());
    }

    @Test
    public void getNurseWithId_100_shouldReturnNotFound_whenNurseWithId_100_isNotInDatabase() throws Exception {
        Long nurseId = 100L;

        this.mockMvc.perform(get("/api/v1/nurses/{id}", nurseId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getNursesByDepartment_shouldReturnOk_whenDepartmentWithId_1_isInDatabase() throws Exception {
        Long departmentId = 1L;

        this.mockMvc.perform(get("/api/v1/nurses/departments/{departmentId}", departmentId))
                .andExpect(status().isOk());
    }

    @Test
    public void getNursesByDepartment_shouldReturnNotFound_whenDepartmentWithId_100_isNotInDatabase() throws Exception {
        Long departmentId = 100L;

        this.mockMvc.perform(get("/api/v1/nurses/departments/{departmentId}", departmentId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createNurse_shouldReturnCreated() throws Exception {
        NurseDto nurse = new NurseDto("Ovidiu Dan", "Asistent medical generalist", "nurse@test.com", 1L);

        this.mockMvc.perform(post("/api/v1/nurses")
                .content(mapper.writeValueAsString(nurse))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void createNurse_shouldReturnNotFound_whenDepartmentWithId_100_isNotInDatabase() throws Exception {
        NurseDto nurse = new NurseDto("Ovidiu Dan", "Asistent medical generalist", "nurse@test.com", 100L);

        this.mockMvc.perform(post("/api/v1/nurses")
                .content(mapper.writeValueAsString(nurse))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateNurseWithId_1_shouldReturnNoContent_whenNurseWithId_1_isInDatabase() throws Exception {
        Long nurseId = 1L;
        NurseDto nurse = new NurseDto("Ovidiu Dan", "Asistent medical generalist", "nurse@test.com", 1L);

        this.mockMvc.perform(put("/api/v1/nurses/{id}", nurseId)
                .content(mapper.writeValueAsString(nurse))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void updateNurseWithId_100_shouldReturnNotFound_whenNurseWithId_100_isNotInDatabase() throws Exception {
        Long nurseId = 100L;
        NurseDto nurse = new NurseDto("Ovidiu Dan", "Asistent medical generalist", "nurse@test.com", 2L);

        this.mockMvc.perform(put("/api/v1/nurses/{id}", nurseId)
                .content(mapper.writeValueAsString(nurse))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateNurseWithId_1_shouldReturnNotFound_whenDepartmentWithId_100_isNotInDatabase() throws Exception {
        Long nurseId = 1L;
        NurseDto nurse = new NurseDto("Ovidiu Dan", "Asistent medical generalist", "nurse@test.com", 100L);

        this.mockMvc.perform(put("/api/v1/nurses/{id}", nurseId)
                .content(mapper.writeValueAsString(nurse))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteNurseWithId_1_shouldReturnNoContent_whenNurseWithId_1_isInDatabase() throws Exception {
        Long nurseId = 1L;

        this.mockMvc.perform(delete("/api/v1/nurses/{id}", nurseId))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteNurseWithId_100_shouldReturnNotFound_whenNurseWithId_100_isNotInDatabase() throws Exception {
        Long nurseId = 100L;

        this.mockMvc.perform(delete("/api/v1/nurses/{id}", nurseId))
                .andExpect(status().isNotFound());
    }
}
