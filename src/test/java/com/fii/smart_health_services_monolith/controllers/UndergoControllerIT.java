package com.fii.smart_health_services_monolith.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fii.smart_health_services_monolith.config.MailSenderTestConfig;
import com.fii.smart_health_services_monolith.dtos.UndergoDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@RunWith(SpringRunner.class)
@Import(MailSenderTestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UndergoControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    @WithUserDetails("admin@gmail.com")
    public void getUndergoesByDoctor_shouldReturnOk_whenDoctorWithId_1_isInDatabase() throws Exception {
        Long doctorId = 1L;

        this.mockMvc.perform(get("/api/v1/undergoes/doctors?doctorId={id}", doctorId))
                .andExpect(status().isOk());
    }

    @Test
    @WithUserDetails("admin@gmail.com")
    public void getUndergoesByDoctor_shouldReturnNotFound_whenDoctorWithId_100_isNotInDatabase() throws Exception {
        Long doctorId = 100L;

        this.mockMvc.perform(get("/api/v1/undergoes/doctors?doctorId={id}", doctorId))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails("admin@gmail.com")
    public void getUndergoesByStay_shouldReturnOk_whenStayWithId_1_isInDatabase() throws Exception {
        Long stayId = 1L;

        this.mockMvc.perform(get("/api/v1/undergoes/stays/{id}", stayId))
                .andExpect(status().isOk());
    }

    @Test
    @WithUserDetails("admin@gmail.com")
    public void getUndergoesByStay_shouldReturnNotFound_whenStayWithId_100_isNotInDatabase() throws Exception {
        Long stayId = 100L;

        this.mockMvc.perform(get("/api/v1/undergoes/stays/{id}", stayId))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails("virgil@gmail.com")
    public void createUndergo_shouldReturnCreated_whenUndergoIsValid() throws Exception {
        UndergoDto undergo = new UndergoDto(1L, 1L, 1L);

        this.mockMvc.perform(post("/api/v1/undergoes")
                .content(mapper.writeValueAsString(undergo))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    @WithUserDetails("virgil@gmail.com")
    public void createUndergo_shouldReturnNotFound_whenProcedureWithId_100_IsNotInDatabase() throws Exception {
        UndergoDto undergo = new UndergoDto(100L, 1L, 1L);

        this.mockMvc.perform(post("/api/v1/undergoes")
                .content(mapper.writeValueAsString(undergo))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails("virgil@gmail.com")
    public void createUndergo_shouldReturnNotFound_whenStayWithId_100_IsNotInDatabase() throws Exception {
        UndergoDto undergo = new UndergoDto(1L, 100L, 1L);

        this.mockMvc.perform(post("/api/v1/undergoes")
                .content(mapper.writeValueAsString(undergo))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails("admin@gmail.com")
    public void deleteUndergoWithId_1_shouldReturnNoContent_whenUndergoWithId_1_isInDatabase() throws Exception {
        Long undergoId = 1L;

        this.mockMvc.perform(delete("/api/v1/undergoes/{id}", undergoId))
                .andExpect(status().isNoContent());
    }

    @Test
    @WithUserDetails("admin@gmail.com")
    public void deleteUndergoWithId_100_shouldReturnNotFound_whenUndergoWithId_100_isNotInDatabase() throws Exception {
        Long undergoId = 100L;

        this.mockMvc.perform(delete("/api/v1/undergoes/{id}", undergoId))
                .andExpect(status().isNotFound());
    }
}
