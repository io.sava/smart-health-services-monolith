package com.fii.smart_health_services_monolith.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fii.smart_health_services_monolith.config.MailSenderTestConfig;
import com.fii.smart_health_services_monolith.dtos.DepartmentDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
@RunWith(SpringRunner.class)
@Import(MailSenderTestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class DepartmentControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void getDepartmentWithId_1_shouldReturnOk_whenDepartmentWithId_1_isInDatabase() throws Exception {
        Long departmentId = 1L;

        this.mockMvc.perform(get("/api/v1/departments/{id}", departmentId))
                .andExpect(status().isOk());
    }

    @Test
    public void getDepartmentWithId_100_shouldReturnNotFound_whenDepartmentWithId_100_isNotInDatabase() throws Exception {
        Long departmentId = 100L;

        this.mockMvc.perform(get("/api/v1/departments/{id}", departmentId))
                .andExpect(status().isNotFound());
    }
    
    @Test
    public void getDepartmentsByHospital_shouldReturnOk_whenHospitalWithId_1_isInDatabase() throws Exception {
        Long hospitalId = 1L;

        this.mockMvc.perform(get("/api/v1/departments/hospitals/{id}", hospitalId))
                .andExpect(status().isOk());
    }

    @Test
    public void getDepartmentsByHospital_shouldReturnNotFound_whenHospitalWithId_100_isNotInDatabase() throws Exception {
        Long hospitalId = 100L;

        this.mockMvc.perform(get("/api/v1/departments/hospitals/{id}", hospitalId))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createDepartment_shouldReturnCreated() throws Exception {
        DepartmentDto department =
                new DepartmentDto("Test department", 1L);

        this.mockMvc.perform(post("/api/v1/departments")
                .content(mapper.writeValueAsString(department))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void updateDepartmentWithId_1_shouldReturnNoContent_whenDepartmentWithId_1_isInDatabase() throws Exception {
        Long departmentId = 1L;
        DepartmentDto department =
                new DepartmentDto("Test department", 1L);

        this.mockMvc.perform(put("/api/v1/departments/{id}", departmentId)
                .content(mapper.writeValueAsString(department))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void updateDepartmentWithId_1_shouldReturnNotFound_whenHospitalWithId_100_isNotInDatabase() throws Exception {
        Long departmentId = 1L;
        DepartmentDto department =
                new DepartmentDto("Test department", 100L);

        this.mockMvc.perform(put("/api/v1/departments/{id}", departmentId)
                .content(mapper.writeValueAsString(department))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateDepartmentWithId_100_shouldReturnNotFound_whenDepartmentWithId_100_isNotInDatabase() throws Exception {
        Long departmentId = 100L;
        DepartmentDto department =
                new DepartmentDto("Test department", 1L);

        this.mockMvc.perform(put("/api/v1/departments/{id}", departmentId)
                .content(mapper.writeValueAsString(department))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteDepartmentWithId_1_shouldReturnNoContent_whenDepartmentWithId_1_isInDatabase() throws Exception {
        Long departmentId = 1L;

        this.mockMvc.perform(delete("/api/v1/departments/{id}", departmentId))
                .andExpect(status().isNoContent());
    }

    @Test
    public void deleteDepartmentWithId_100_shouldReturnNotFound_whenDepartmentWithId_100_isNotInDatabase() throws Exception {
        Long departmentId = 100L;

        this.mockMvc.perform(delete("/api/v1/departments/{id}", departmentId))
                .andExpect(status().isNotFound());
    }
}
